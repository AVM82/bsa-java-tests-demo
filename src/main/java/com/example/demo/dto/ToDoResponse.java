package com.example.demo.dto;

import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

public class ToDoResponse {
    @NotNull
    public Long id;

    @NotNull
    public String text;

    public ZonedDateTime completedAt;

    @Override
    public String toString() {
        return "ToDoResponse{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", completedAt=" + completedAt +
                '}';
    }
}