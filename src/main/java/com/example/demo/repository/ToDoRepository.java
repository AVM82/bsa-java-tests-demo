package com.example.demo.repository;

import com.example.demo.model.ToDoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ToDoRepository extends JpaRepository<ToDoEntity, Long> {

    @Query("select t from ToDoEntity t where t.completedAt is not null")
    List<ToDoEntity> getAllCompleted();
}
