package com.example.demo.controller;

import com.example.demo.dto.ToDoSaveRequest;
import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;
import com.example.demo.service.ToDoService;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Optional;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(ToDoController.class)
@ActiveProfiles(profiles = "test")
@Import(ToDoService.class)
public class ToDoControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ToDoRepository toDoRepository;

    @Test
    void whenGetAll_ThenReturnResponse() throws Exception {

        //given
        var expected = new ArrayList<ToDoEntity>();
        expected.add(new ToDoEntity(1L, "do nothing"));
        expected.add(new ToDoEntity(2L, "do nothing one more time").completeNow());

        given(this.toDoRepository.findAll()).willReturn(expected);

        //then
        this.mockMvc.perform(get("/todos").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").value(expected.get(0).getId()))
                .andExpect(jsonPath("$[0].text").value(expected.get(0).getText()))
                .andExpect(jsonPath("$[0].completedAt").doesNotExist())
                .andExpect(jsonPath("$[1].id").value(expected.get(1).getId()))
                .andExpect(jsonPath("$[1].text").value(expected.get(1).getText()))
                .andExpect(jsonPath("$[1].completedAt").exists());
    }

    @Test
    void whenSaveWithoutId_ThenAddNew() throws Exception {

        //given
        var expected = new ToDoEntity(0L, "do nothing");

        given(this.toDoRepository.save(ArgumentMatchers.any(ToDoEntity.class))).willAnswer(i -> {
            ToDoEntity arg = i.getArgument(0);
            if (arg.getId() == null) {
                return expected;
            } else {
                return new ToDoEntity();
            }
        });

        //then
        mockMvc.perform(post("/todos").contentType(MediaType.APPLICATION_JSON)
                .content("{\"text\": \"Do nothing\"}")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(expected.getId()))
                .andExpect(jsonPath("$.text").value(expected.getText()))
                .andExpect(jsonPath("$.completedAt").doesNotExist());
    }

    @Test
    void whenSaveWithInvalidId_ThenException() throws Exception {

        //given
        given(this.toDoRepository.findById(anyLong())).willReturn(Optional.empty());

        //then
        mockMvc.perform(post("/todos").contentType(MediaType.APPLICATION_JSON)
                .content("{\"id\": 1,\"text\": \"Do nothing\"}")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string("Can not find todo with id 1"));
    }

    @Test
    void whenSaveWithValidId_ThenUpdated() throws Exception {

        //given
        final long id = 0L;
        String oldTask = "Do something";
        String newTask = "Do nothing";

        var toDoSave = new ToDoSaveRequest();
        toDoSave.id = id;
        toDoSave.text = newTask;

        var expected = new ToDoEntity(0L, oldTask);

        given(this.toDoRepository.findById(id)).willAnswer(i -> {
            long arg = i.getArgument(0);
            if (arg == id) {
                return Optional.of(expected);
            } else {
                return Optional.empty();
            }
        });

        given(this.toDoRepository.save(expected)).willAnswer(i -> {
            expected.setText(newTask);
            return expected;
        });

        //then
        mockMvc.perform(post("/todos").contentType(MediaType.APPLICATION_JSON)
                .content("{\"id\": 0,\"text\": \"" + newTask + "\"}")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(expected.getId()))
                .andExpect(jsonPath("$.text").value(newTask))
                .andExpect(jsonPath("$.completedAt").doesNotExist());
    }

    @Test
    void whenCompleteWithValidID_ThenReturnUpdatedToDoEntity() throws Exception {

        //given
        long id = 0L;
        String text = "do something";
        var expected = new ToDoEntity(id, text);

        given(this.toDoRepository.findById(id)).willAnswer(i -> {
            long arg = i.getArgument(0);
            if (arg == id) {
                return Optional.of(expected);
            } else {
                return Optional.empty();
            }
        });

        given(this.toDoRepository.save(expected)).willAnswer(i -> {
            expected.completeNow();
            return expected;
        });

        //then
        mockMvc.perform(put("/todos/{id}/complete", id).contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(expected.getId()))
                .andExpect(jsonPath("$.text").value(text))
                .andExpect(jsonPath("$.completedAt").exists());

    }

    @Test
    void whenCompleteWithInvalidID_ThenException() throws Exception {

        //given
        given(this.toDoRepository.findById(anyLong())).willReturn(Optional.empty());

        //then
        mockMvc.perform(put("/todos/{id}/complete", anyLong()).contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string("Can not find todo with id 0"));
    }

    @Test
    public void whenGetOne_ThenReturnOne() throws Exception {
        //given
        long id = anyLong();
        var expected = new ToDoEntity(id, "Do something");
        given(toDoRepository.findById(id)).willAnswer(i -> {
            long arg = i.getArgument(0);
            if (arg == id) {
                return Optional.of(expected);
            } else return Optional.empty();
        });

        //then
        this.mockMvc.perform(get("/todos/{id}", id).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(expected.getId()))
                .andExpect(jsonPath("$.text").value(expected.getText()))
                .andExpect(jsonPath("$.completedAt").doesNotExist());
    }

    @Test
    public void whenGetOneWithInvalidId_ThenException() throws Exception {
        //given
        given(toDoRepository.findById(anyLong())).willReturn(Optional.empty());

        //then
        this.mockMvc.perform(get("/todos/{id}", anyLong()).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string("Can not find todo with id 0"));
    }

    @Test
    public void whenDeleteOne() throws Exception {
        //given
        long id = anyLong();

        //then
        this.mockMvc.perform(delete("/todos/{id}", id)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void whenGetAllCompleted_ThenReturnResponse() throws Exception {

        //given
        var expected = new ArrayList<ToDoEntity>();
        expected.add(new ToDoEntity(2L, "do nothing one more time").completeNow());

        given(this.toDoRepository.getAllCompleted()).willReturn(expected);

        //then
        this.mockMvc.perform(get("/todos/completed").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(expected.get(0).getId()))
                .andExpect(jsonPath("$[0].text").value(expected.get(0).getText()))
                .andExpect(jsonPath("$[0].completedAt").exists());
    }

    @Test
    void whenGetNoCompleted_ThenReturnEmpty() throws Exception {

        //given
        var expected = new ArrayList<ToDoEntity>();

        given(this.toDoRepository.getAllCompleted()).willReturn(expected);

        //then
        this.mockMvc.perform(get("/todos/completed").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(0)));
    }

}
