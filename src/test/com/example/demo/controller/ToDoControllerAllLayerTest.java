package com.example.demo.controller;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ToDoControllerAllLayerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    @Order(1)
    public void getAllTest() throws Exception {
        this.mockMvc.perform(get("/todos").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].text").value("Wash the dishes"))
                .andExpect(jsonPath("$[0].completedAt").doesNotExist())
                .andExpect(jsonPath("$[1].id").value(2))
                .andExpect(jsonPath("$[1].text").value("Learn to test Java app"))
                .andExpect(jsonPath("$[1].completedAt").exists());
    }

    @Test
    @Order(2)
    public void saveTest() throws Exception {

        mockMvc.perform(post("/todos").contentType(MediaType.APPLICATION_JSON)
                .content("{\"id\": 3,\"text\": \"Do nothing\"}")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string("Can not find todo with id 3"));

        mockMvc.perform(post("/todos").contentType(MediaType.APPLICATION_JSON)
                .content("{\"text\": \"do something\"}")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(3))
                .andExpect(jsonPath("$.text").value("do something"))
                .andExpect(jsonPath("$.completedAt").doesNotExist());

        mockMvc.perform(post("/todos").contentType(MediaType.APPLICATION_JSON)
                .content("{\"id\": 3, \"text\": \"do something else\"}")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(3))
                .andExpect(jsonPath("$.text").value("do something else"))
                .andExpect(jsonPath("$.completedAt").doesNotExist());

        mockMvc.perform(put("/todos/{id}/complete", 100500).contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string("Can not find todo with id 100500"));

        mockMvc.perform(put("/todos/{id}/complete", 3).contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(3))
                .andExpect(jsonPath("$.text").value("do something else"))
                .andExpect(jsonPath("$.completedAt").exists());

    }

    @Test
    @Order(3)
    public void deleteOneTest() throws Exception {
        this.mockMvc.perform(delete("/todos/{id}", 3)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @Order(4)
    public void getAllCompletedTest() throws Exception {
        this.mockMvc.perform(get("/todos/completed").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(2))
                .andExpect(jsonPath("$[0].text").value("Learn to test Java app"))
                .andExpect(jsonPath("$[0].completedAt").exists());
    }

}
