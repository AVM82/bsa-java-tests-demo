package com.example.demo.service;

import com.example.demo.dto.ToDoResponse;
import com.example.demo.dto.ToDoSaveRequest;
import com.example.demo.exception.ToDoNotFoundException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;


@SpringBootTest
@AutoConfigureMockMvc
public class ToDoServiceIT {

    @Autowired
    private ToDoService toDoService;

    @Test
    public void getByIdTest() throws ToDoNotFoundException {
        //given
        ToDoSaveRequest toDoDTO = new ToDoSaveRequest();
        String someText = "someText";
        toDoDTO.text = someText;
        var newToDo = toDoService.upsert(toDoDTO);

        //when
        var response = toDoService.getOne(newToDo.id);

        //then
        assertThat(response.id).isEqualTo(newToDo.id);
        assertThat(response.text).isEqualTo(someText);
    }

    @Test
    public void deleteOneTest() throws ToDoNotFoundException {
        //given
        ToDoSaveRequest toDoDTO = new ToDoSaveRequest();
        toDoDTO.text = "someText 2";
        var newToDo = toDoService.upsert(toDoDTO);
        int toDoSizeBeforeDelete = toDoService.getAll().size();

        //when
        toDoService.deleteOne(newToDo.id);
        Throwable thrown = assertThrows(ToDoNotFoundException.class, () -> toDoService.getOne(newToDo.id));

        //then
        int toDoSizeAfterDelete = toDoService.getAll().size();
        assertThat(toDoSizeAfterDelete).isLessThan(toDoSizeBeforeDelete);
        assertNotNull(thrown.getMessage());
    }

    @Test
    public void getAllCompletedTest() {
        //given

        //when
        List<ToDoResponse> actual = toDoService.getAllCompleted();

        //then
        assertThat(actual.size()).isEqualTo(1);
        assertThat(actual.get(0).completedAt).isNotNull();

    }

}
