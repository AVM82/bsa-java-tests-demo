package com.example.demo.service;

import com.example.demo.dto.ToDoResponse;
import com.example.demo.dto.ToDoSaveRequest;
import com.example.demo.dto.mapper.ToDoEntityToResponseMapper;
import com.example.demo.exception.ToDoNotFoundException;
import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class ToDoServiceTest {

    private ToDoService toDoService;
    private ToDoRepository toDoRepository;

    @BeforeEach
    void setUp() {
        this.toDoRepository = mock(ToDoRepository.class);
        toDoService = new ToDoService(toDoRepository);
    }


    @Test
    public void whenGetAll_thenReturnAll() {
        //given
        var expected = new ArrayList<ToDoEntity>();
        expected.add(new ToDoEntity(1L, "do nothing"));
        expected.add(new ToDoEntity(2L, "do nothing one more time").completeNow());

        when(toDoRepository.findAll()).thenReturn(expected);

        //when
        var actual = toDoService.getAll();

        //then
        verify(toDoRepository, times(1)).findAll();
        assertThat(actual, is(not(empty())));
        assertEquals(expected.size(), actual.size());
        for (int i = 0; i < actual.size(); i++) {
            ToDoResponse mapExpected = ToDoEntityToResponseMapper.map(expected.get(i));
            assertThat(actual.get(i).id, is(mapExpected.id));
            assertThat(actual.get(i).text, is(mapExpected.text));
        }
    }

    @Test
    public void whenGetAll_ThenReturnEmpty() {

        //given
        when(toDoRepository.findAll()).thenReturn(new ArrayList<>());

        //when
        var actual = toDoService.getAll();

        //then
        verify(toDoRepository, times(1)).findAll();
        assertThat(actual, is(empty()));

    }

    @Test
    public void whenUpsertWithoutId_ThenAddNew() throws ToDoNotFoundException {
        //given
        String task = "do nothing";
        var toDoSave = new ToDoSaveRequest();
        toDoSave.text = task;

        var expected = new ToDoEntity(0L, task);
        when(toDoRepository.save(ArgumentMatchers.any(ToDoEntity.class))).thenAnswer(i -> {
            ToDoEntity arg = i.getArgument(0);
            if (arg.getId() == null) {
                return expected;
            } else {
                return new ToDoEntity();
            }
        });

        //when
        var actual = toDoService.upsert(toDoSave);

        //then
        verify(toDoRepository, times(1)).save(ArgumentMatchers.any(ToDoEntity.class));
        ToDoResponse mapExpected = ToDoEntityToResponseMapper.map(expected);
        assertThat(actual.id, is(mapExpected.id));
        assertThat(actual.text, is(mapExpected.text));
    }

    @Test
    public void whenUpsertWithId_ThenUpdate() throws ToDoNotFoundException {
        //given
        final long id = 0L;
        String oldTask = "Do something";
        String newTask = "Do nothing";
        var toDoSave = new ToDoSaveRequest();
        toDoSave.id = id;
        toDoSave.text = newTask;
        var expected = new ToDoEntity(0L, oldTask);
        when(toDoRepository.findById(id)).thenReturn(Optional.of(expected));

        when(toDoRepository.save(expected)).thenAnswer(i -> {
            expected.setText(newTask);
            return expected;
        });

        //when
        var actual = toDoService.upsert(toDoSave);

        //then
        verify(toDoRepository, times(1)).findById(id);
        verify(toDoRepository, times(1)).save(ArgumentMatchers.any(ToDoEntity.class));
        ToDoResponse mapExpected = ToDoEntityToResponseMapper.map(expected);
        assertThat(actual.id, is(mapExpected.id));
        assertThat(actual.text, is(mapExpected.text));
    }

    @Test
    public void whenUpsertWithIdButNotFound_ThenToDoNotFoundException() {
        //given
        var toDoSave = new ToDoSaveRequest();
        final long id = 0L;
        toDoSave.id = id;
        toDoSave.text = "do something";
        when(toDoRepository.findById(id)).thenReturn(Optional.empty());

        //when
        Throwable thrown = assertThrows(ToDoNotFoundException.class, () -> toDoService.upsert(toDoSave));

        //then
        verify(toDoRepository, times(1)).findById(id);
        verify(toDoRepository, times(0)).save(ArgumentMatchers.any(ToDoEntity.class));
        assertNotNull(thrown.getMessage());
    }

    @Test
    public void whenCompleteWithValidID_ThenReturnUpdatedToDoEntity() throws ToDoNotFoundException {

        //given
        var time = ZonedDateTime.now(ZoneOffset.UTC);
        long id = 0L;
        var expected = new ToDoEntity(id, "do something");
        when(toDoRepository.findById(id)).thenAnswer(i -> {
            long arg = i.getArgument(0);
            if (arg == id) {
                return Optional.of(expected);
            } else {
                return Optional.empty();
            }
        });
        when(toDoRepository.save(expected)).thenReturn(expected);

        //when
        var actual = toDoService.completeToDo(id);

        //then
        verify(toDoRepository, times(1)).findById(id);
        verify(toDoRepository, times(1)).save(expected);
        assertNotNull(actual.completedAt);
        assertTrue(actual.completedAt.isAfter(time));
    }

    @Test
    public void whenCompleteWithInvalidID_ThenToDoNotFoundException() {

        //given
        long id = anyLong();
        when(toDoRepository.findById(id)).thenReturn(Optional.empty());

        //when
        Throwable thrown = assertThrows(ToDoNotFoundException.class, () -> toDoService.completeToDo(id));

        //then
        verify(toDoRepository, times(1)).findById(id);
        verify(toDoRepository, times(0)).save(ArgumentMatchers.any(ToDoEntity.class));
        assertNotNull(thrown.getMessage());
    }

    @Test
    public void whenGetOne_ThenReturnOne() throws ToDoNotFoundException {
        //given
        long id = anyLong();
        var expected = new ToDoEntity(id, "Do something");
        when(toDoRepository.findById(id)).thenAnswer(i -> {
            long arg = i.getArgument(0);
            if (arg == id) {
                return Optional.of(expected);
            } else return Optional.empty();
        });

        //when
        var actual = toDoService.getOne(id);

        //then
        verify(toDoRepository, times(1)).findById(id);

        ToDoResponse mapExpected = ToDoEntityToResponseMapper.map(expected);
        assertThat(actual.id, is(mapExpected.id));
        assertThat(actual.text, is(mapExpected.text));
    }

    @Test
    public void whenGetOneWithInvalidId_ThenToDoNotFoundException() {
        //given
        long id = anyLong();
        when(toDoRepository.findById(id)).thenReturn(Optional.empty());

        //when
        Throwable thrown = assertThrows(ToDoNotFoundException.class, () -> toDoService.getOne(id));

        //then
        verify(toDoRepository, times(1)).findById(id);
        assertNotNull(thrown.getMessage());
    }

    @Test
    public void whenDeleteOne() {
        //given
        long id = anyLong();

        //when
        toDoService.deleteOne(id);

        //then
        verify(toDoRepository, times(1)).deleteById(id);

    }

    @Test
    public void whenGetAllCompleted_ThenReturnRespond() {
        //given
        var expected = new ArrayList<ToDoEntity>();
        expected.add(new ToDoEntity(2L, "do nothing one more time").completeNow());

        when(toDoRepository.getAllCompleted()).thenReturn(expected);

        //when
        var actual = toDoService.getAllCompleted();

        //then
        verify(toDoRepository, times(1)).getAllCompleted();
        assertThat(actual, is(not(empty())));
        assertEquals(expected.size(), actual.size());
        for (int i = 0; i < actual.size(); i++) {
            ToDoResponse mapExpected = ToDoEntityToResponseMapper.map(expected.get(i));
            assertThat(actual.get(i).id, is(mapExpected.id));
            assertThat(actual.get(i).text, is(mapExpected.text));
            assertNotNull(actual.get(i).completedAt);
        }
    }

    @Test
    public void whenNoGetAllCompleted_ThenEmpty() {
        //given
        var expected = new ArrayList<ToDoEntity>();

        when(toDoRepository.getAllCompleted()).thenReturn(expected);

        //when
        var actual = toDoService.getAllCompleted();

        //then
        verify(toDoRepository, times(1)).getAllCompleted();
        assertThat(actual, is(empty()));
        assertEquals(expected.size(), actual.size());
    }
}
