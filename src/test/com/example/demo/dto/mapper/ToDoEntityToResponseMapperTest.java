package com.example.demo.dto.mapper;

import com.example.demo.dto.ToDoResponse;
import com.example.demo.model.ToDoEntity;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertNull;

public class ToDoEntityToResponseMapperTest {


    @Test
    public void whenMap_ThenReturnToDoResponse() {

        //given
        long id = 15L;
        String text = "text";
        ToDoEntity toDoEntity = new ToDoEntity(id, text);

        var expected = new ToDoResponse();
        expected.id = id;
        expected.text = text;

        //when
        var actual = ToDoEntityToResponseMapper.map(toDoEntity);

        //then
        assertThat(actual.id, is(expected.id));
        assertThat(actual.text, is(expected.text));
        assertNull(actual.completedAt);

    }
}
